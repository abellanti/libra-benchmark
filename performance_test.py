import argparse
import os
import subprocess
import random
import time

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="This tool will analyze a libra blockchain or order to give you "
                                                 "insight about performance")

    parser.add_argument("port", type=int, help="Port which is used by the libra testnet")
    parser.add_argument("libra_folder", type=str, help="folder in which you cloned the libra repo")
    parser.add_argument("mint_file", type=str, help="mint.key file path (given by libra swarm)")
    parser.add_argument("waypoint", type=str, help="waypoint for the blockchain (given by libra swarm)")
    parser.add_argument("chain_id", type=str, help="chain-id (given by libra swarm)")
    parser.add_argument("n_transactions", type=int, help="Number of transactions for each round")
    parser.add_argument("-r", "--n_rounds", type=int, default=2, help="Number of rounds")
    parser.add_argument("-n", "--n_accounts", type=int, default=2, help="Number of accounts that will be created")

    args = parser.parse_args()
    working_folder = os.getcwd()

    with open("preparing-env.txt", "w") as fp:
        for i in range(0, args.n_accounts):
            fp.write(f"account create\naccount mint {i} 1000000 LBR\n")
        fp.write(f"account write {working_folder}/mnemonic")

    print("Preparing env; please wait")

    with open("mnemonic_generator.sh", "w") as fp:
        fp.write("#!/bin/bash\n")
        fp.write(f"cd {args.libra_folder}\n")
        fp.write(f'cargo run --bin cli -- -u http://localhost:{args.port} -m {args.mint_file} --waypoint {args.waypoint} --chain-id {args.chain_id} < {working_folder}/preparing-env.txt\n')

    process = subprocess.run(["bash", "mnemonic_generator.sh"])

    print("Done. Initializing transactions")

    with open("mnemonic_generator.sh", "w") as fp:
        fp.write("#!/bin/bash\n")
        fp.write(f"cd {args.libra_folder}\n")
        fp.write(f'cargo run --bin cli -- -u http://localhost:{args.port} -m {args.mint_file} --waypoint {args.waypoint} --chain-id {args.chain_id} < {working_folder}/tester.txt')

    times = list()

    for i in range(0, args.n_rounds):
        with open("tester.txt", "w") as fp:
            fp.write(f"account recover {working_folder}/mnemonic\n")
            for j in range(0, int(args.n_transactions)):
                sender = 0
                receiver = 0
                amount = random.randint(1, 10)
                while sender == receiver:
                    sender = random.randint(0, (args.n_accounts-1))
                    receiver = random.randint(0, (args.n_accounts-1))
                fp.write(f"transferb {sender} {receiver} {amount} LBR\n")
            fp.write(f"account write {working_folder}/mnemonic")
        start_time = time.time()
        process = subprocess.run(["bash", "mnemonic_generator.sh"])
        times.append((time.time() - start_time))
        time.sleep(30)

    i = 1
    with open("results/speed/time_dimensions.csv", "w") as fp:
        fp.write("round, time (s), avg time for transaction (s)")
        for t in times:
            fp.write(f"\n{i}, {t}, {t/args.n_transactions}")
            i += 1
