# Premesse
Questa guida permette di avviare in locale una rete di validatori Libra e di lanciare gli script di test presenti in questo repository.
La guida si basa su alcune premesse, è balida solo per sistemi Ubuntu ed è stata testata unicamente su Ubuntu 20:

* git è installato sul sistema
* apt-get è installato sul sistema e funzionante
* sul sistema sono installati python3 e pip

# Clone e build di Libra
Per eseguire la build di libra è necessario eseguire questi passaggi:

* clone del repository libra con il comando: `git clone https://github.com/libra/libra.git && cd libra`
* checkout del branch testnet con il comando: `git checkout testnet`
* Installazione delle dipendenze con: `./scripts/dev_setup.sh`

# Creazione di una rete con uno o più validatori utilizzando libra_swarm
Per creare una rete di uno o due validatori Libra, aprire nel terminale la cartella creata precedentemente e utilizzare il comando:
`cargo run -p libra-swarm`

Questo comando, di default genera una rete di 4 validatori.
Alcune opzioni che possono tornare utili ai fini dei test sono:

* `-n` che permette di specificare il numero di validatori
* `-c` che permette di specificare un percorso in cui i file di Libra saranno salvati in maniera non volatile

**NB**: queste opzioni vanno sempre utilizzate postfisse rispetto al comando e separate da --
Per esempio, per lanciare una rete con un solo validatore il comando sarà: `cargo run -p libra-swarm -- -n 1`

# Utilizzo degli script di benchmark
Prima di lanciare uno qualsiasi degli script, è necessario avviare la testnet utilizzando il comando: `cargo run -p libra-swarm` con le relative opzoini.
Una volta completato il processo di lancio, si avrà un output di questo tipo:
```
To run the Libra CLI client in a separate process and connect to the validator nodes you just spawned, use this command:
	cargo run --bin cli -- -u http://localhost:39855 -m "/tmp/65af4f175a2c2c7ca6cca2195f1df428/mint.key" --waypoint 0:ddcb5cf5326249efb1802b222695312f593b835e329cde4a6d97816d307a4f51 --chain-id 4
To run transaction generator run:
	cargo run -p cluster-test -- --mint-file "/tmp/65af4f175a2c2c7ca6cca2195f1df428/mint.key" --swarm --peers "localhost:39855" --emit-tx --workers-per-ac 1
To run health check:
	cargo run -p cluster-test -- --mint-file "/tmp/65af4f175a2c2c7ca6cca2195f1df428/mint.key" --swarm --peers "localhost:39855:40883" --health-check --duration 30
CTRL-C to exit.
```
Quest' output è molto importante in quanto ci fornirà dei parametri che ci serviranno per lanciare i nostri script

## Misurazione della quantità di spazio occupata da un validatore
Questo test permette di lanciare un numero personalizzato di transazioni e di salvare in un file csv lo spazio disco occupato dal validatore.
Per lanciare lo script si può utilizzare il comando:

`python3 dimension_test.py <porta> <directory in cui è stato clonato il repository Libra> <file mint.key> <waypoint> <chain_id> <numero di transazioni per round> <directory in cui si trovano i dati della blockchain attuale> -r <numero di round> -n <numero di account utilizzati>`

In sostanza, dato l'output riportato in precedenza, si dovrebbe richiamare:

`python3 dimension_test.py 39855 <directory in cui è stato clonato il repository Libra> /tmp/65af4f175a2c2c7ca6cca2195f1df428/mint.key 0:ddcb5cf5326249efb1802b222695312f593b835e329cde4a6d97816d307a4f51 4 <numero di transazioni per round> /tmp/65af4f175a2c2c7ca6cca2195f1df428/ -r <numero di round> -n <numero di account utilizzati>`

I dati che sono rimasti come placehoders sono dipendenti dalla singola installazione o dalle esigenze di chi lancia lo script.

## Misurazione della velocità di esecuzione delle transazioni (single thread)
Questo test permette di misurare quanto tempo occorre a un certo numero di transazioni per essere eseguito e salva i risultati in formato csv
Per lanciare lo script si può usare il comando:

`python3 performance_test.py <porta> <directory in cui è stato clonato il repository Libra> <file mint.key> <waypoint> <chain_id> <numero di transazioni per round> <directory in cui si trovano i dati della blockchain attuale> -r <numero di round> -n <numero di account utilizzati>` 

Quindi, sempre utilizzando come riferimento il nostro validatore, avremo:

`python3 performance_test.py 39855 <directory in cui è stato clonato il repository Libra> /tmp/65af4f175a2c2c7ca6cca2195f1df428/mint.key 0:ddcb5cf5326249efb1802b222695312f593b835e329cde4a6d97816d307a4f51 4 <numero di transazioni per round> /tmp/65af4f175a2c2c7ca6cca2195f1df428/ -r <numero di round> -n <numero di account utilizzati>`

## Misurazione della velocità di esecuzione delle transazioni (multi thread)
Questo test permette di misurare quanto tempo occorre a un certo numero di transazioni per essere eseguito e salva i risultati in formato csv
Per lanciare lo script si può usare il comando:

`python3 multithread_performance_test.py <porta> <directory in cui è stato clonato il repository Libra> <file mint.key> <waypoint> <chain_id> <numero di transazioni per round> <directory in cui si trovano i dati della blockchain attuale> -r <numero di round> -n <numero di account utilizzati> -t <numero di thread utilizzati>` 

Quindi, sempre utilizzando come riferimento il nostro validatore, avremo:

`python3 multithread_performance_test.py 39855 <directory in cui è stato clonato il repository Libra> /tmp/65af4f175a2c2c7ca6cca2195f1df428/mint.key 0:ddcb5cf5326249efb1802b222695312f593b835e329cde4a6d97816d307a4f51 4 <numero di transazioni per round> /tmp/65af4f175a2c2c7ca6cca2195f1df428/ -r <numero di round> -n <numero di account utilizzati> -t <numero di thread utilizzati>`

## Misurazione della velocità di esecuzione delle transazioni (multi thread) (non bloccante)
Questo test è identico al precedente; l'unica differenza risiede nel fatto che l'unica transazione bloccante è l'ultima, rendendo di fatto le transazioni eseguite parallelamente.
Per lanciare lo script si può usare il comando:

`python3 non_blocking_multithread_performance_test.py <porta> <directory in cui è stato clonato il repository Libra> <file mint.key> <waypoint> <chain_id> <numero di transazioni per round> <directory in cui si trovano i dati della blockchain attuale> -r <numero di round> -n <numero di account utilizzati> -t <numero di thread utilizzati>` 

Quindi, inserendo i dati del nostro validatore avremo:

`python3 non_blocking_multithread_performance_test.py 39855 <directory in cui è stato clonato il repository Libra> /tmp/65af4f175a2c2c7ca6cca2195f1df428/mint.key 0:ddcb5cf5326249efb1802b222695312f593b835e329cde4a6d97816d307a4f51 4 <numero di transazioni per round> /tmp/65af4f175a2c2c7ca6cca2195f1df428/ -r <numero di round> -n <numero di account utilizzati> -t <numero di thread utilizzati>`
